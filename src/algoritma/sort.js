let data = [8, 2, 20, -7, 25, -1, 5];

function sort(data) {
  let temp;

  for (let i = 0; i < data.length; i++) {
    for (let j = i; j < data.length; j++) {
      if (data[i] > data[j]) {
        // swap
        temp = data[i];
        data[i] = data[j];
        data[j] = temp;
      }
    }
  }
  return data;
}

console.log(sort(data));
