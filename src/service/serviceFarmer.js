export const getDataServiceFarmer = async () => {
  const url = "https://api.eratani.co.id/getAboutFarmer";

  try {
    const response = await fetch(url, {
      method: "POST",
    });
    return response.json();
  } catch (error) {
    console.log(error);
    throw error;
  }
};
