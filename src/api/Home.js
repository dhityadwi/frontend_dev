import React from "react";
import { useEffect, useState } from "react/cjs/react.development";
import { getDataServiceFarmer } from "../service/serviceFarmer";

export const Home = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    getDataServiceFarmer()
      .then((response) => {
        setData(response.data);
        // console.log(response, "respon data");
        console.log(data, "data");
        // console.log(setData, "inisetdata");
      })
      .catch((error) => {
        console.log(error);
      });
  }, [data]);

  return (
    <>
      <div className="containerHome">
        <div className="daftarNama">
          <h2>List Title Nama </h2>
          {data.map((item) => {
            return (
              <div className="list-api">
                <div>
                  <ul>
                    <li>{item.title}</li>
                  </ul>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};
