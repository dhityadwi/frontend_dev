import "./App.css";
import Navbar from "./Component/Navbar";
import { Even } from "./Component/Event";
import { Footer } from "./Component/Footer";
import { Home } from "./api/Home";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Even />
      <Footer />

      <Home />
    </div>
  );
}

export default App;
