import React from "react";
import "./Event.css";
import event1 from "../assets/event_01.png";
import event2 from "../assets/event_02.png";
import event3 from "../assets/event_03.png";
import event4 from "../assets/event_04.png";
import event5 from "../assets/event_05.png";
import cal from "../assets/ic_calendar.png";

export const Even = () => {
  return (
    <>
      <div className="header"></div>
      <div className="kegiatan-cont">
        <br />
        <br />
        <h2>Kegiatan</h2>
        <br />
        <br />
        <div>
          <div className="news-event">
            <div className="foto">
              <img src={event1} alt="tdl" />
            </div>
            <div className="info">
              <div className="tgl">
                <img src={cal} alt="cal" />
                <span>9 November 2021</span>
              </div>
              <div className="isi">
                <h1>Sosialisasi Pemanfaatan Sampah Plastik di Indramayu</h1>
                <p>
                  Sampah plastik menjadi masalah lingkungan yang telah mengakar
                  sejak lama di Indonesia. Banyaknya pemakaian produk dengan
                  berbahan dasar plastik menjadikan jumlah sampah plastik di
                  Indonesia membludak. Sampah plastik juga menjadi salah satu
                  jenis sampah yang sulit terurai. Itulah mengapa Eratani ingin
                  memberikan pengetahuan kepada para petani binaan di Indramayu
                  terkait bagaimana cara memanfaatkan sampah plastik yang ada.
                  Sosialisasi diawali dengan kegiatan mengumpulkan sampah
                  berbahan dasar plastik secara bersama-sama. Kemudian sampah
                  yang ada dipilah dan ditimbang untuk kemudian ditukar dengan
                  reward yang telah disediakan oleh tim Eratani. Setelah
                  terkumpul, para petani diarahkan untuk membuat karya
                  menggunakan sampah plastik tersebut menjadi Eco-Brick yang
                  mampu dimanfaatkan secara berkelanjutan. Melalui sosialisasi
                  pemanfaatan sampah plastik ini, Eratani berharap bisa
                  mendorong para petani binaan untuk bisa lebih produktif demi
                  mengurangi sampah plastik di lingkungan sekitar.
                </p>
                <div className="lengkap">Baca Selengkapnya</div>
              </div>
            </div>
          </div>
          <br />
          <br />
        </div>

        <br />
        <br />
        <div>
          <div className="news-event">
            <div className="foto">
              <img src={event2} alt="tdl" />
            </div>
            <div className="info">
              <div className="tgl">
                <img src={cal} alt="cal" />
                <span>9 November 2021</span>
              </div>
              <div className="isi">
                <h1>Sosialisasi Pemanfaatan Sampah Plastik di Indramayu</h1>
                <p>
                  Sampah plastik menjadi masalah lingkungan yang telah mengakar
                  sejak lama di Indonesia. Banyaknya pemakaian produk dengan
                  berbahan dasar plastik menjadikan jumlah sampah plastik di
                  Indonesia membludak. Sampah plastik juga menjadi salah satu
                  jenis sampah yang sulit terurai. Itulah mengapa Eratani ingin
                  memberikan pengetahuan kepada para petani binaan di Indramayu
                  terkait bagaimana cara memanfaatkan sampah plastik yang ada.
                  Sosialisasi diawali dengan kegiatan mengumpulkan sampah
                  berbahan dasar plastik secara bersama-sama. Kemudian sampah
                  yang ada dipilah dan ditimbang untuk kemudian ditukar dengan
                  reward yang telah disediakan oleh tim Eratani. Setelah
                  terkumpul, para petani diarahkan untuk membuat karya
                  menggunakan sampah plastik tersebut menjadi Eco-Brick yang
                  mampu dimanfaatkan secara berkelanjutan. Melalui sosialisasi
                  pemanfaatan sampah plastik ini, Eratani berharap bisa
                  mendorong para petani binaan untuk bisa lebih produktif demi
                  mengurangi sampah plastik di lingkungan sekitar.
                </p>
                <div className="lengkap">Baca Selengkapnya</div>
              </div>
            </div>
          </div>
          <br />
          <br />
        </div>

        <br />
        <br />
        <div>
          <div className="news-event">
            <div className="foto">
              <img src={event3} alt="tdl" />
            </div>
            <div className="info">
              <div className="tgl">
                <img src={cal} alt="cal" />
                <span>9 November 2021</span>
              </div>
              <div className="isi">
                <h1>Sosialisasi Pemanfaatan Sampah Plastik di Indramayu</h1>
                <p>
                  Sampah plastik menjadi masalah lingkungan yang telah mengakar
                  sejak lama di Indonesia. Banyaknya pemakaian produk dengan
                  berbahan dasar plastik menjadikan jumlah sampah plastik di
                  Indonesia membludak. Sampah plastik juga menjadi salah satu
                  jenis sampah yang sulit terurai. Itulah mengapa Eratani ingin
                  memberikan pengetahuan kepada para petani binaan di Indramayu
                  terkait bagaimana cara memanfaatkan sampah plastik yang ada.
                  Sosialisasi diawali dengan kegiatan mengumpulkan sampah
                  berbahan dasar plastik secara bersama-sama. Kemudian sampah
                  yang ada dipilah dan ditimbang untuk kemudian ditukar dengan
                  reward yang telah disediakan oleh tim Eratani. Setelah
                  terkumpul, para petani diarahkan untuk membuat karya
                  menggunakan sampah plastik tersebut menjadi Eco-Brick yang
                  mampu dimanfaatkan secara berkelanjutan. Melalui sosialisasi
                  pemanfaatan sampah plastik ini, Eratani berharap bisa
                  mendorong para petani binaan untuk bisa lebih produktif demi
                  mengurangi sampah plastik di lingkungan sekitar.
                </p>
                <div className="lengkap">Baca Selengkapnya</div>
              </div>
            </div>
          </div>
          <br />
          <br />
        </div>
        <br />
        <br />
        <div>
          <div className="news-event">
            <div className="foto">
              <img src={event4} alt="tdl" />
            </div>
            <div className="info">
              <div className="tgl">
                <img src={cal} alt="cal" />
                <span>9 November 2021</span>
              </div>
              <div className="isi">
                <h1>Sosialisasi Pemanfaatan Sampah Plastik di Indramayu</h1>
                <p>
                  Sampah plastik menjadi masalah lingkungan yang telah mengakar
                  sejak lama di Indonesia. Banyaknya pemakaian produk dengan
                  berbahan dasar plastik menjadikan jumlah sampah plastik di
                  Indonesia membludak. Sampah plastik juga menjadi salah satu
                  jenis sampah yang sulit terurai. Itulah mengapa Eratani ingin
                  memberikan pengetahuan kepada para petani binaan di Indramayu
                  terkait bagaimana cara memanfaatkan sampah plastik yang ada.
                  Sosialisasi diawali dengan kegiatan mengumpulkan sampah
                  berbahan dasar plastik secara bersama-sama. Kemudian sampah
                  yang ada dipilah dan ditimbang untuk kemudian ditukar dengan
                  reward yang telah disediakan oleh tim Eratani. Setelah
                  terkumpul, para petani diarahkan untuk membuat karya
                  menggunakan sampah plastik tersebut menjadi Eco-Brick yang
                  mampu dimanfaatkan secara berkelanjutan. Melalui sosialisasi
                  pemanfaatan sampah plastik ini, Eratani berharap bisa
                  mendorong para petani binaan untuk bisa lebih produktif demi
                  mengurangi sampah plastik di lingkungan sekitar.
                </p>
                <div className="lengkap">Baca Selengkapnya</div>
              </div>
            </div>
          </div>
          <br />
          <br />
        </div>
        <br />
        <br />
        <div>
          <div className="news-event">
            <div className="foto">
              <img src={event5} alt="tdl" />
            </div>
            <div className="info">
              <div className="tgl">
                <img src={cal} alt="cal" />
                <span>9 November 2021</span>
              </div>
              <div className="isi">
                <h1>Sosialisasi Pemanfaatan Sampah Plastik di Indramayu</h1>
                <p>
                  Sampah plastik menjadi masalah lingkungan yang telah mengakar
                  sejak lama di Indonesia. Banyaknya pemakaian produk dengan
                  berbahan dasar plastik menjadikan jumlah sampah plastik di
                  Indonesia membludak. Sampah plastik juga menjadi salah satu
                  jenis sampah yang sulit terurai. Itulah mengapa Eratani ingin
                  memberikan pengetahuan kepada para petani binaan di Indramayu
                  terkait bagaimana cara memanfaatkan sampah plastik yang ada.
                  Sosialisasi diawali dengan kegiatan mengumpulkan sampah
                  berbahan dasar plastik secara bersama-sama. Kemudian sampah
                  yang ada dipilah dan ditimbang untuk kemudian ditukar dengan
                  reward yang telah disediakan oleh tim Eratani. Setelah
                  terkumpul, para petani diarahkan untuk membuat karya
                  menggunakan sampah plastik tersebut menjadi Eco-Brick yang
                  mampu dimanfaatkan secara berkelanjutan. Melalui sosialisasi
                  pemanfaatan sampah plastik ini, Eratani berharap bisa
                  mendorong para petani binaan untuk bisa lebih produktif demi
                  mengurangi sampah plastik di lingkungan sekitar.
                </p>
                <div className="lengkap">Baca Selengkapnya</div>
              </div>
            </div>
          </div>
          <br />
          <br />
        </div>
      </div>
    </>
  );
};
