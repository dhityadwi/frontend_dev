import React, { useState } from "react";
import whiteLogo from "../assets/logo-white.png";
import colorLogo from "../assets/logo_eratani.png";
import globe from "../assets/ic_globe.png";
import "./NavBar.css";

const Navbar = () => {
  const [navbar, setNavbar] = useState(false);

  const changeBg = () => {
    if (window.scrollY >= 80) {
      setNavbar(true);
    } else {
      setNavbar(false);
    }
  };

  window.addEventListener("scroll", changeBg);

  return (
    <>
      <div className="main-nav active">
        <div className="container">
          <nav className={navbar ? "navbar active" : "navbar"}>
            <a href="#a" className="navbar-logo">
              <div className="logo active">
                <img src={navbar ? colorLogo : whiteLogo} alt="logo" />
              </div>
            </a>
            <div className="menu-nav">
              <ul className="menu">
                <li>Beranda</li>
                <li>Tentang Kami</li>
                <li>Tips & Berita Pertanian</li>
                <li>Kegiatan</li>
                <li>FAQ</li>
              </ul>
              <div className="menu-kanan">
                <img src={globe} alt="bola" />
                <span>ID</span>
                <span>EN</span>
                <div className="daftar">Mitra Petani</div>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </>
  );
};

export default Navbar;
