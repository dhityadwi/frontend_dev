import React from "react";
import "./Footer.css";
import whiteLogo from "../assets/logo-white.png";
import ig from "../assets/icon_ig.png";
import linked from "../assets/icon_linkedin.png";
import tiktok from "../assets/icon_tiktok.png";
import wa from "../assets/icon_wa.png";
import yutub from "../assets/icon_youtube.png";

export const Footer = () => {
  return (
    <>
      <di className="footer">
        {/* <Footer> */}
        <div className="containerF">
          <div className="menu-add">
            <div className="add">
              <div>
                <img src={whiteLogo} alt="log" />
              </div>
              <p>
                Jl. Casablanca Raya Kav 88, Kel. Menteng Dalam, Kec. Tebet,
                Gedung Pakuwon Tower Lt 26 Unit J, Jakarta Selatan, DKI Jakarta
                12870, Indonesia{" "}
              </p>
              <p>Email : info@eratani.co.id</p>
              <p>Telepon : +62 811 952 2577</p>
            </div>
            <div className="menu_f">
              <h4>Menu</h4>
              <div>Beranda</div>
              <div>Tim Kami</div>
              <div>Tips & Berita Pertanian</div>
              <div>Kegiatan</div>
              <div>FAQ</div>
            </div>
          </div>
          <div className="sosmed">
            <div>
              <img src={tiktok} alt="tt" />
            </div>
            <div>
              <img src={ig} alt="tt" />
            </div>
            <div>
              <img src={linked} alt="tt" />
            </div>
            <div>
              <img src={yutub} alt="tt" />
            </div>
            <div>
              <img src={wa} alt="tt" />
            </div>
          </div>
          <p className="copy">
            Copyright © 2021 by PT Eratani Teknologi Nusantara
          </p>
        </div>
        {/* </Footer> */}
      </di>
    </>
  );
};
